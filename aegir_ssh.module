<?php

/**
 * Implements hook_nodeapi()
 *
 * Loads ALL uploaded ssh keys into the server_master server node.
 */
function aegir_ssh_node_load($nodes, $types) {
  if (count(array_intersect(array('server'), $types)) == 0) {
    return;
  }

  // Only save keys for @hostmaster's own web server.
  foreach ($nodes as &$node) {
    if ($node->nid == variable_get('hosting_own_web_server', NULL)) {
      $node->authorized_keys = array();
      $query = db_query('SELECT value FROM {sshkey}');
      while ($result = $query->fetchObject()) {
        $node->authorized_keys[] = $result->value;
      }
    }
  }
}

/**
 * Implements hook_sshkey_insert()
 *
 * When an SSH Key is added, trigger server verify.
 *
 * @param $key
 *   The SSH key object that was created.
 */
function aegir_ssh_sshkey_insert($key) {
  aegir_ssh_server_verify();
}

/**
 * Implements hook_sshkey_update()
 *
 * When an SSH Key is updated, trigger server verify.
 *
 * @param $key
 *   The SSH key object that was updated.
 */
function aegir_ssh_sshkey_update($key) {
  aegir_ssh_server_verify();
}

/**
 * Implements hook_sshkey_delete()
 *
 * When an SSH Key is deleted, trigger server verify.
 *
 * @param $key
 *   The SSH key object that was updated.
 */
function aegir_ssh_sshkey_delete($key) {
  aegir_ssh_server_verify();
}

/**
 * Implements hook_user_cancel().
 */
function aegir_ssh_user_cancel($edit, $account, $method) {
  aegir_ssh_server_verify();
}

/**
 * Implements hook_user_delete().
 */
function aegir_ssh_user_delete($account) {
  aegir_ssh_server_verify();
}

/**
 * Helper to trigger server verify.
 */
function aegir_ssh_server_verify() {
  $nid = variable_get('hosting_own_web_server', NULL);

  if ($nid) {
    hosting_add_task($nid, 'verify');
    drupal_set_message(t('Your key is being added to the server. You should be able to connect once verification is completed.'));
  }
}